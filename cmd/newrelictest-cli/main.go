package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"text/tabwriter"

	"bitbucket.org/RpJ/newrelictest"
)

func main() {
	log.SetFlags(0)
	log.SetPrefix("newrelictest-cli: ")

	var (
		tokenFlag    = flag.String("token", "", "Github personal token.")
		locationFlag = flag.String("location", "", "Location constraint (city name).")
		limitFlag    = flag.Int("limit", 150, "Number of results.")
	)
	flag.Parse()

	if *tokenFlag == "" {
		log.Fatal("A valid Github token is needed.")
	}

	if *locationFlag == "" {
		log.Fatal("A valid location is needed.")
	}

	nrt := newrelictest.NewNewRelicTest()

	g := &newrelictest.GithubClient{
		Token: *tokenFlag,
	}

	users, err := nrt.GetTopGithubContributors(g, *locationFlag, *limitFlag)
	if err != nil {
		log.Fatal(err)
	}

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.TabIndent)
	fmt.Fprintln(w, "Key\tLogin\tRepositories\t")
	for k, user := range users {
		str := fmt.Sprintf("%d\t%s\t%d\t", k+1, user.Login, user.PublicRepos)
		fmt.Fprintln(w, str)
	}
	w.Flush()
}
