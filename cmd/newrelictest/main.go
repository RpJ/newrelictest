package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"bitbucket.org/RpJ/newrelictest"
)

func homeRoute(w http.ResponseWriter, r *http.Request) {
	queryParams := r.URL.Query()

	// get the location parameter ( if it is not provided fails )
	location := queryParams.Get("location")
	if location == "" {
		http.Error(w, "HTTP 400 BadRequest: A location must be provided", http.StatusBadRequest)
		return
	}

	// get the limit parameter ( if it is not provided use 150 as default )
	slimit := queryParams.Get("limit")
	if location == "" {
		slimit = "150"
	}
	limit, err := strconv.Atoi(slimit)
	if err != nil {
		limit = 150
	}

	nrt := newrelictest.NewNewRelicTest()

	g := &newrelictest.GithubClient{
		Token: os.Getenv("GITHUBTOKEN"),
	}

	users, err := nrt.GetTopGithubContributors(g, location, limit)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsondata, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, string(jsondata))
}

func getTokenRoute(w http.ResponseWriter, r *http.Request) {
	secretKey := os.Getenv("SECRETKEY")

	// generate a simple token
	token := jwt.New(jwt.GetSigningMethod("HS256"))
	tokenString, _ := token.SignedString([]byte(secretKey))
	fmt.Fprintf(w, tokenString)
}

func authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		secretKey := os.Getenv("SECRETKEY")

		// get header Authorization
		authorizationHeader := r.Header.Get("Authorization")
		if authorizationHeader == "" {
			http.Error(w, "HTTP 401 Unauthorized", http.StatusUnauthorized)
			return
		}
		// should be formated as "Authorization: JWT <jwt.string>"
		// 					  or "Authorization: Bearer <jwt.string>"
		authorizationHeaderParts := strings.Split(authorizationHeader, " ")
		if len(authorizationHeaderParts) != 2 {
			http.Error(w, "HTTP 401 Unauthorized", http.StatusUnauthorized)
			return
		}
		if authorizationHeaderParts[0] != "Bearer" && authorizationHeaderParts[0] != "JWT" {
			http.Error(w, "HTTP 401 Unauthorized", http.StatusUnauthorized)
			return
		}
		tokenString := authorizationHeaderParts[1]

		// validate token string
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if !token.Valid {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func main() {
	log.SetFlags(0)
	log.SetPrefix("newrelictest: ")

	var (
		serverAddressPtr = flag.String("address", "", "Server address")
		serverPortPtr    = flag.String("port", "8080", "Server port")
	)
	flag.Parse()

	homeHandler := http.HandlerFunc(homeRoute)

	http.Handle("/", authMiddleware(homeHandler))

	http.HandleFunc("/generate-token", getTokenRoute)

	address := fmt.Sprintf("%s:%s", *serverAddressPtr, *serverPortPtr)
	log.Println("Start server at", address, "...")

	http.ListenAndServe(":8080", nil)
}
