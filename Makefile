PACKAGES = "./..."

# Build

install:
	go install $(LDFLAGS) -v $(PACKAGES)

.PHONY: clean
clean:
	go clean
	rm -rf coverage-all.out

# Dependencies
deps:
	go get -u github.com/dgrijalva/jwt-go

dev-deps:
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install

# Testing

.PHONY: test
test:
	go test -v $(PACKAGES)

.PHONY: cover
cover:
	go test -cover $(PACKAGES)

.PHONY: cover-html
cover-html:
	echo "mode: count" > coverage-all.out
	$(foreach pkg,$(shell go list ./...),\
		go test -coverprofile=coverage.out -covermode=count $(pkg);\
		tail -n +2 coverage.out >> coverage-all.out;)
	rm -rf coverage.out
	go tool cover -html=coverage-all.out

# Documentation

godoc-serve:
	godoc -http=":9090"