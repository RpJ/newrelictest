package newrelictest

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

// FakeGithubClient ...
type FakeGithubClient struct {
}

// FetchURL ...
func (g *FakeGithubClient) FetchURL(url string) ([]byte, error) {
	result := `{}`

	if url == `https://api.github.com/search/users?q=repos:%3E0+location:Barcelona&sort=repositories&order=desc&page=1&per_page=100` {
		result = `{  
		   "total_count":2,
		   "incomplete_results":false,
		   "items":[
			  {  
				 "login":"github",
				 "id":9919,
				 "avatar_url":"https://avatars3.githubusercontent.com/u/9919?v=4",
				 "gravatar_id":"",
				 "url":"https://api.github.com/users/github",
				 "html_url":"https://github.com/github",
				 "followers_url":"https://api.github.com/users/github/followers",
				 "following_url":"https://api.github.com/users/github/following{/other_user}",
				 "gists_url":"https://api.github.com/users/github/gists{/gist_id}",
				 "starred_url":"https://api.github.com/users/github/starred{/owner}{/repo}",
				 "subscriptions_url":"https://api.github.com/users/github/subscriptions",
				 "organizations_url":"https://api.github.com/users/github/orgs",
				 "repos_url":"https://api.github.com/users/github/repos",
				 "events_url":"https://api.github.com/users/github/events{/privacy}",
				 "received_events_url":"https://api.github.com/users/github/received_events",
				 "type":"User",
				 "site_admin":false,
				 "score":1
			  },
			  {  
				"login":"octocat",
				"id":583231,
				"avatar_url":"https://avatars3.githubusercontent.com/u/583231?v=4",
				"gravatar_id":"",
				"url":"https://api.github.com/users/octocat",
				"html_url":"https://github.com/octocat",
				"followers_url":"https://api.github.com/users/octocat/followers",
				"following_url":"https://api.github.com/users/octocat/following{/other_user}",
				"gists_url":"https://api.github.com/users/octocat/gists{/gist_id}",
				"starred_url":"https://api.github.com/users/octocat/starred{/owner}{/repo}",
				"subscriptions_url":"https://api.github.com/users/octocat/subscriptions",
				"organizations_url":"https://api.github.com/users/octocat/orgs",
				"repos_url":"https://api.github.com/users/octocat/repos",
				"events_url":"https://api.github.com/users/octocat/events{/privacy}",
				"received_events_url":"https://api.github.com/users/octocat/received_events",
				"type":"User",
				"site_admin":false,
				"score":1
			 }
		   ]
		}`
	}

	if url == `https://api.github.com/users/octocat` {
		result = `{
			"login": "octocat",
			"id": 583231,
			"avatar_url": "https://avatars3.githubusercontent.com/u/583231?v=4",
			"gravatar_id": "",
			"url": "https://api.github.com/users/octocat",
			"html_url": "https://github.com/octocat",
			"followers_url": "https://api.github.com/users/octocat/followers",
			"following_url": "https://api.github.com/users/octocat/following{/other_user}",
			"gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
			"starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
			"subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
			"organizations_url": "https://api.github.com/users/octocat/orgs",
			"repos_url": "https://api.github.com/users/octocat/repos",
			"events_url": "https://api.github.com/users/octocat/events{/privacy}",
			"received_events_url": "https://api.github.com/users/octocat/received_events",
			"type": "User",
			"site_admin": false,
			"name": "The Octocat",
			"company": "GitHub",
			"blog": "http://www.github.com/blog",
			"location": "San Francisco",
			"email": null,
			"hireable": null,
			"bio": null,
			"public_repos": 7,
			"public_gists": 8,
			"followers": 1949,
			"following": 5,
			"created_at": "2011-01-25T18:44:36Z",
			"updated_at": "2017-09-23T04:10:39Z"
		  }`
	}

	if url == `https://api.github.com/users/github` {
		result = `{
			"login": "github",
			"id": 9919,
			"avatar_url": "https://avatars1.githubusercontent.com/u/9919?v=4",
			"gravatar_id": "",
			"url": "https://api.github.com/users/github",
			"html_url": "https://github.com/github",
			"followers_url": "https://api.github.com/users/github/followers",
			"following_url": "https://api.github.com/users/github/following{/other_user}",
			"gists_url": "https://api.github.com/users/github/gists{/gist_id}",
			"starred_url": "https://api.github.com/users/github/starred{/owner}{/repo}",
			"subscriptions_url": "https://api.github.com/users/github/subscriptions",
			"organizations_url": "https://api.github.com/users/github/orgs",
			"repos_url": "https://api.github.com/users/github/repos",
			"events_url": "https://api.github.com/users/github/events{/privacy}",
			"received_events_url": "https://api.github.com/users/github/received_events",
			"type": "Organization",
			"site_admin": false,
			"name": "GitHub",
			"company": null,
			"blog": "https://github.com/about",
			"location": "San Francisco, CA",
			"email": "support@github.com",
			"hireable": null,
			"bio": "How people build software.",
			"public_repos": 197,
			"public_gists": 0,
			"followers": 0,
			"following": 0,
			"created_at": "2008-05-11T04:37:31Z",
			"updated_at": "2017-09-09T04:09:40Z"
		  }`
	}

	return []byte(result), nil
}

func TestFetchURL(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Hello, client")
	}))
	defer ts.Close()
	g := GithubClient{
		Token: "token",
	}
	_, err := g.FetchURL(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
}

func TestFetchURLNotStatusOK(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintln(w, `{
			"message": "Bad credentials",
			"documentation_url": "https://developer.github.com/v3"
		}`)
	}))
	defer ts.Close()
	g := GithubClient{
		Token: "invalid-token",
	}
	_, err := g.FetchURL(ts.URL)
	if err.Error() != "401 Unauthorized" {
		t.Fatal(err)
	}
}

func TestFetchURLInvalidURL(t *testing.T) {
	url := "invalid"
	g := GithubClient{
		Token: "token",
	}
	_, err := g.FetchURL(url)
	if err.Error() != `Get invalid: unsupported protocol scheme ""` {
		t.Fatal(err)
	}
}
