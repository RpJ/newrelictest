# New Relic Coding Test

This is a coding test for New Relic.

Information about the work needed to do and the solution can be found at
*QUESTION.md* and *SOLUTION.md* files respectively.

## Table of contents

* [Installation](#markdown-header-installation)
* [Quick start](#markdown-header-quick-start)
* [Usage](#markdown-header-usage)
  * [REST API service](#markdown-header-rest-api-service)
  * [CLI interface](#markdown-header-cli-interface)

## Installation

Install it as a usual golang package:

```bash
go get bitbucket.org/RpJ/newrelictest
```

#### Building from sources

To build fron sources first fork and clone using your user in your golang 
workspace.
Then build the project using the taks `make deps`, `make install`, `make test` and 
`make cover` provided by the Makefile.

## Quick start

1. Get a Github Personal API token from [here](https://github.com/settings/tokens/new)
2. Execute the REST API Service server.
  ```
  $ GITHUBTOKEN="<token>" SECRETKEY="newrelicsecretkey" newrelictest
  ``` 
3. Generate an Authorization Token with:
  ```
  $ curl -s http://localhost:8080/generate-token
  ```
4. Call the REST API service:
  ```
  $ curl -s -H "Authorization: Bearer <token>" "http://localhost:8080?location=Barcelona&limit=50"
  ```


## Usage

This project provides two different interfaces a REST API service and a CLI
tool:

### REST API service

This interface starts an HTTP server on a desired port and address that exposes
a simple REST API.

```bash
$ newrelictest
Usage of newrelictest:
  -address string
        Server address
  -port string
        Server port (default "8080")
```

Additionally and following the 12 factor apps guidelines the REST API service
relies on two environment variables:

###### SECRETKEY

The value of this variable is Used to generate and validate JWT tokens, wich 
is the authentication mechanism of this API.

Notice that this secret key should not be shared or made public and that's
the reason why it is read from the environment.

###### GITHUBTOKEN

The value of this variabler is used to perform authorized calls to the
Gihub API. To obtain a token refer to the Github documentation about
[personal tokens](https://github.com/settings/tokens) or follow
[this link](https://github.com/settings/tokens/new).

#### Example

Server:

```bash
$ GITHUBTOKEN="<token>" SECRETKEY="<secret>" newrelictest
newrelictest: Start server at :8080 ...
```

Client:

First you need to generate a valid token to call the API:

```bash
$ curl -s "http://localhost:8080/generate-token"
```

And then:

```bash
$ curl -s -H "Authorization: Bearer <token>" "http://localhost:8080?location=Barcelona&limit=5"
```

### CLI interface

```bash
$ newrelictest-cli --help
Usage of newrelictest-cli:
  -limit int
        Number of results. (default 150)
  -location string
        Location constraint (city name).
  -token string
        Github personal token.
```

#### Example

```bash
$ # Notice that the token argument is hidden on purpose.
$ newrelic-test --token <token> --location Barcelona --limit 5
Key Login                   Repositories
1   kristianmandrup         1015
2   leobcn                  596
3   albodelu                398
4   nilportugues            363
5   ardock                  350
```
