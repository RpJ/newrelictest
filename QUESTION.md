# Code Challenge for Backend Engineer

The goal of this exercise is to write an HTTP service with the following specs:

## Service specs

* Given a city name (e.g. Barcelona) the service returns a list of the top
  contributors (sorted by number of repositories) in GitHub.
* The service should give the possibility to choose between the Top 50, Top 100
  or Top 150 contributors.
* The service can be implemented using any set of technologies (language,
  framework, libraries).
* Paying attention to concurrency and other scalability issues will be highly
  appreciated.
* Provide clear instructions on how to launch the service.

## Optional feature

Please make sure you’ve spent enough time on the mandatory features first, this
is really where we want to evaluate your expertise in software engineering.

Then, if you’re still eager to code, implement an auth mechanism to protect the
service.

There is no time limit to complete the exercise, we want to remove any pressure
as much as possible. The goal is to have an idea of your skills, it’s not an
exam. But please, try not to take more than a week to complete the challenge
and to send it back to us.

If you have doubts, please contact us. The formulation is by no means
academical.

Hope you'll have fun working on the exercise, we're eager to see your answer!