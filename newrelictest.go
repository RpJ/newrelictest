package newrelictest

import (
	"encoding/json"
	"sort"
	"strconv"
)

// GithubUser ...
type GithubUser struct {
	Login       string `json:"login"`
	PublicRepos int    `json:"public_repos"`
}

// ByPublicRepos ...
type byPublicRepos []*GithubUser

func (a byPublicRepos) Len() int           { return len(a) }
func (a byPublicRepos) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byPublicRepos) Less(i, j int) bool { return a[i].PublicRepos < a[j].PublicRepos }

// searchGithubUserResult ...
type searchGithubUserResult struct {
	Items []struct {
		URL string `json:"url"`
	} `json:"items"`
}

// NewRelicTest ...
type NewRelicTest struct {
	GithubToken string
}

// NewNewRelicTest ...
func NewNewRelicTest() *NewRelicTest {
	nrt := &NewRelicTest{}
	return nrt
}

// GetTopGithubContributors ...
func (nrt *NewRelicTest) GetTopGithubContributors(g Github, location string, limit int) ([]*GithubUser, error) {
	// search for github users by location ordered by number off repositories
	// returns an slice of urls
	urls, err := nrt.searchUsers(g, location, limit)
	if err != nil {
		return nil, err
	}

	// get information for each user
	users, err := nrt.getUserRepos(g, urls)
	if err != nil {
		return nil, err
	}

	sort.Sort(sort.Reverse(byPublicRepos(users)))

	return users, nil
}

func buildSearcUsersURL(location string, page int) string {
	p := page + 1
	baseURL := "https://api.github.com/search/users"
	q := "repos:%3E0+location:" + location
	sort := "repositories"
	order := "desc"
	spage := strconv.Itoa(p)
	sperpage := "100"
	url := baseURL + "?q=" + q + "&sort=" + sort + "&order=" + order + "&page=" + spage + "&per_page=" + sperpage
	return url
}

// searchUsers ...
func (nrt *NewRelicTest) searchUsers(g Github, location string, limit int) ([]string, error) {
	var urls []string

	// calculate number of pages (github only returns 100 items per page)
	pages := (limit / 100) + 1

	ch := make(chan *searchGithubUserResult, pages)

	errch := make(chan error, 1)

	// get each in in paralel
	for i := 0; i < pages; i++ {
		url := buildSearcUsersURL(location, i)
		go func() {
			data, err := g.FetchURL(url)
			if err != nil {
				errch <- err
			}
			var sresults searchGithubUserResult
			_ = json.Unmarshal(data, &sresults)
			ch <- &sresults
		}()
	}

	for i := 0; i < pages; i++ {
		select {
		case sresults := <-ch:
			for _, v := range sresults.Items {
				urls = append(urls, v.URL)
			}
		case err := <-errch:
			return urls, err
		}
	}

	if len(urls) > limit {
		urls = urls[:limit]
	}

	return urls, nil
}

// getUserRepos ...
func (nrt *NewRelicTest) getUserRepos(g Github, urls []string) ([]*GithubUser, error) {
	var users []*GithubUser

	ch := make(chan *GithubUser, len(urls))

	errch := make(chan error, 1)

	// get each in paralel
	for i := range urls {
		url := urls[i]
		go func() {
			data, err := g.FetchURL(url)
			if err != nil {
				errch <- err
			}
			var user GithubUser
			_ = json.Unmarshal(data, &user)
			ch <- &user
		}()
	}

	for i := 0; i < len(urls); i++ {
		select {
		case user := <-ch:
			users = append(users, user)
		case err := <-errch:
			return users, err
		}
	}

	return users, nil
}
