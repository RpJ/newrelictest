package newrelictest

import (
	"testing"
)

func TestBuildSearcUsersURL(t *testing.T) {
	expected := "https://api.github.com/search/users?q=repos:%3E0+location:Barcelona&sort=repositories&order=desc&page=2&per_page=100"
	location := "Barcelona"
	page := 1
	url := buildSearcUsersURL(location, page)
	if expected != url {
		t.Fatalf("Expected %s but got %s", expected, url)
	}
}

func TestNewNewRelicTest(t *testing.T) {
	nrt := NewNewRelicTest()
	if nrt == nil {
		t.Fatalf("Expected an instance but got nil")
	}
}

func TestGetTopGithubContributors(t *testing.T) {
	github := &FakeGithubClient{}
	nrt := NewNewRelicTest()
	location := "Barcelona"
	limit := 2
	contributors, err := nrt.GetTopGithubContributors(github, location, limit)
	if err != nil {
		t.Fatalf("Expected result with no error but got %v", err)
	}
	if len(contributors) != limit {
		t.Fatalf("Expected result to be an slice of %d elements but got %d", limit, len(contributors))
	}
}

func TestGetTopGithubContributorsLessThanAsked(t *testing.T) {
	github := &FakeGithubClient{}
	nrt := NewNewRelicTest()
	location := "Barcelona"
	limit := 1
	contributors, err := nrt.GetTopGithubContributors(github, location, limit)
	if err != nil {
		t.Fatalf("Expected result with no error but got %v", err)
	}
	if len(contributors) != limit {
		t.Fatalf("Expected result to be an slice of %d elements but got %d", limit, len(contributors))
	}
}
