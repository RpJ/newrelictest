package newrelictest

import (
	"errors"
	"io/ioutil"
	"net/http"
)

// Github ...
type Github interface {
	FetchURL(url string) ([]byte, error)
}

// GithubClient ...
type GithubClient struct {
	Token string
}

// FetchURL ...
func (g *GithubClient) FetchURL(url string) ([]byte, error) {
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "token "+g.Token)

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return data, errors.New(resp.Status)
	}

	return data, nil
}
